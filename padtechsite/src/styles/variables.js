export const commonVariables = {
  // Fonts
  padtech: 'Montserrat',

  // Margins


  // Dimensions


  // Basic Colors
  greyPadtech: '#72757b',
  bluePadtech: '#396cb2',
  yellowPadtech: '#ffbd59',
  white: '#FFFFFF',
  black: '#000000',
}