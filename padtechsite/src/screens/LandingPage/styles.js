import { commonVariables } from '../../styles/variables'

const styles = (props) => {

  return ( 
    {
      desktop: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-end',
          width: '100%',
          height: '10vh'
        },
        content: {
          display: 'flex',
          flexDirection: 'row',
          width: '100%',
        },
        leftSideAboveContainer: {
          display: 'flex',
          flexDirection: 'column',
          width: '60vw',
          marginLeft: '5vw',
        },
        leftSideBellowContainer: {
          display: 'flex',
          flexDirection: 'row',
          marginTop: '5vh'
        },
        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '62px',
          color: commonVariables?.greyPadtech,
          marginBottom: '1vh',
          fontWeight: '500',
          letterSpacing: '5px',
          zIndex: 10
        },
        subtitle: {
          fontFamily: commonVariables?.padtech,
          fontSize: '28px',
          color: commonVariables?.black,
          marginTop: '0',
          fontWeight: '200',
          letterSpacing: '5px',
          zIndex: 10
        },
        consultingTextContainer: {
          display: 'flex',
          justifyContent: 'flex-end',
          marginLeft: '-3vw',
          marginTop: '-2vh'
        },
        consultingText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '34px',
          width: '60%',
          color: commonVariables?.greyPadtech,
          fontWeight: '300'
        },
        arrowContainer: {
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center',
          marginLeft: '5vw',
          marginTop: '-7vh',
        },
        videoContainer: {
          display: 'flex',
          alignItems: 'center',
          marginLeft: '-15vw',
          marginTop: '20vh'
        },
        bypadtech: {
          fontFamily: commonVariables?.padtech,
          fontSize: '24px',
          color: commonVariables?.greyPadtech,
          margin: '2vh',
          fontWeight: '300'
        },
        bypadtechBlue: {
          fontFamily: commonVariables?.padtech,
          fontSize: '24px',
          color: commonVariables?.bluePadtech,
          fontWeight: '300'
        }
      },


    }
  )
}

export default styles