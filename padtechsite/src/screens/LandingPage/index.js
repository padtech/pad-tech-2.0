import React from 'react'
import { useStyles } from 'react-styles-hook'

import Banner from '../../components/Banner/index'
import AboutUs from '../../components/AboutUs/index'
import WhatWeDo from '../../components/WhatWeDo/index'
import GridCards from '../../components/GridCards/index'
import OurProccess from '../../components/OurProccess/index'
import Footer from '../../components/Footer/index'

import styles from './styles'

const LandingPage = (props) => {
  
  const screenStyle = useStyles(styles(props))

  return (
    <div style={screenStyle?.desktop?.mainContainer}>
      <Banner />
      <AboutUs />
      <WhatWeDo />
      <GridCards />
      <OurProccess />
      <Footer />
    </div>
  )
}

export default LandingPage