import * as actionTypes from './actionTypes'

export const startRequest = () => {
  return (dispatch) => {
    dispatch({
      type: actionTypes.START_REQUEST,
    })

    fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then((res) => {
      dispatch({
        type: actionTypes.START_REQUEST_SUCCESS,
        res
      })
    })
    .catch((err) => {
      dispatch({
        type: actionTypes.START_REQUEST_FAIL,
        err
      })
    })
  }
}