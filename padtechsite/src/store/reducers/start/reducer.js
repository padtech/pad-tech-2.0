import * as actionTypes from './actionTypes'
import { createReducer } from '@reduxjs/toolkit'

const initialState = {
  loaded: null,
  loading: null,
  error: null
}

function get(state) {
  state.loaded = false
  state.loading = true
  state.error = null
}

function getError(state, action) {
  state.loaded = false
  state.loading = false
  state.error = action.error
}

const startReducer = createReducer({}, {
  [actionTypes.START_REQUEST]: get,
  [actionTypes.START_REQUEST_FAIL]: getError,
  [actionTypes.START_REQUEST_SUCCESS]: (state, action) => {
    // const {
    //   accessToken,
    // } = action?.result

    state.loaded = true
    state.loading = false
    state.error = false

  },
})

export default function reducer(state = initialState, action = {}) {
  return {
    start: startReducer(state, action)
  }
}