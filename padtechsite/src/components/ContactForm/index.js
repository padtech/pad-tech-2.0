import React, { useState } from 'react'

import { useHistory } from 'react-router-dom'

import { useResponsive } from '../../hooks/responsive'

import './styles.css'

const ContactForm = () => {

  const { desktop, tabletLandscape, tablet, mobile } = useResponsive()

  const [contentToEmail, setContentToEmail] = useState("")

  const history = useHistory()

  const href = "mailto:contact@padtech.io?subject = Fabricar minha ideia&body = Entro em contato para podemos conversar sobre meu projeto. Mensagem: " + contentToEmail 

  const handleDesktop = () => (
    <div class="box">
      <form>
        <div class="contact-box">
          <input
            type="text"
            name=""
            required=""
            placeholder={'Digite aqui'}
            onChange={e => setContentToEmail(e.target.value)}
          />
        </div>
          <a onSubmit={() => history.push('/#/')} href={`mailto:contact@padtech.io?Subject=Fabricar minha ideia&body=Entro em contato para podemos conversar sobre meu projeto. \n Mensagem: ${contentToEmail}`}>
            <span></span>
            <span></span>
            Enviar
          </a>
      </form>
    </div>
  )

  const handleTablet = () => (
    <div class="footer">
      <h2 class="title-tablet">Quer fabricar a sua ideia?</h2>
      <h2 class="subtitle-tablet">Conte-nos sobre...</h2>
      <div class="box-tablet">
        
        <form>
          <div class="contact-box-tablet">
            <input
              type="text"
              name=""
              required=""
              placeholder={'Digite aqui'}
              onChange={e => setContentToEmail(e.target.value)}
            />
          </div>
            <a onSubmit={() => history.push('/#/')} href={`mailto:contact@padtech.io?Subject=Fabricar minha ideia&body=Entro em contato para podemos conversar sobre meu projeto. \n Mensagem: ${contentToEmail}`}>
              <span></span>
              <span></span>
              Enviar
            </a>
        </form>
      </div>
    </div>
  )

  const handleMobile = () => (
    <div class="footer">
      <h2 class="title">Quer fabricar a sua ideia?</h2>
      <h2 class="subtitle">Conte-nos sobre...</h2>
      <div class="box-mobile">
        
        <form>
          <div class="contact-box-mobile">
            <input
              type="text"
              name=""
              required=""
              placeholder={'Digite aqui'}
              value={contentToEmail}
              onChange={e => setContentToEmail(e.target.value)}
            />
          </div>
          <a href={`https://api.whatsapp.com/send?phone=+351912015235&text=${contentToEmail}`}>
            <span></span>
            <span></span>
            Enviar
          </a>
        </form>
      </div>
    </div>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  } else {
    return handleMobile()
  }
}

export default ContactForm