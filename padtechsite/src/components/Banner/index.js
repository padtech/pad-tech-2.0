import React from 'react'
import ReactPlayer from 'react-player'
import { useStyles } from 'react-styles-hook'

import { useResponsive } from '../../hooks/responsive'

import qrcode from '../../assets/images/qrcode.png'
import videoLevelUp from '../../assets/videos/videolevelUp.mp4'

import styles from './styles'
import './styles.css'


const Banner = (props) => {
  const screenStyle = useStyles(styles(props))

  const { desktop, tabletLandscape, tablet, mobile } = useResponsive()

  const handleDesktop = () => (
    <>
      <div style={screenStyle?.desktop?.navbar}>
        <p style={screenStyle?.desktop?.bypadtech}>
          by <span style={screenStyle?.desktop?.bypadtechBlue}>pad</span>tech
        </p>
      </div>

      <div style={screenStyle?.desktop?.content}>
        <div>
          <div style={screenStyle?.desktop?.leftSideAboveContainer}>
            <h1 style={screenStyle?.desktop?.title}>
              Eleve sua empresa ao pr<span style={{color: '#ffbd59'}}>ó</span>ximo nível
            </h1>

            <h4 style={screenStyle?.desktop?.subtitle}>SOFTWARES. SOLUÇÕES. IDEIAS.</h4>
          </div>

          <div style={screenStyle?.desktop?.leftSideBellowContainer}>
            <div>

              <div style={screenStyle?.desktop?.consultingTextContainer}>
                <p style={screenStyle?.desktop?.consultingText}>
                  agende sua c<span style={{color: '#ffbd59'}}>o</span>nsultoria
                </p>
              </div> 

              <div style={screenStyle?.desktop?.arrowContainer}>
                <svg class="arrows">
                  <path class="a1" d="M0 0 L15 25 L30 0"></path>
                  <path class="a2" d="M0 20 L15 45 L30 20"></path>
                  <path class="a3" d="M0 40 L15 65 L30 40"></path>
                </svg>
              </div>

            </div>

            <div>
              <img src={qrcode} alt={'QR Code para email'} style={{height: '30vh'}}/>
            </div>
          </div>
        </div>

        <div style={screenStyle?.desktop?.videoContainer}>
          <ReactPlayer url={videoLevelUp} loop={true} playing controls={true} />
        </div>
      </div>
    </>
  )

  const handleTabletLandscape = () => (
    <>
      <div style={screenStyle?.desktop?.navbar}>
        <p style={screenStyle?.desktop?.bypadtech}>
          by <span style={screenStyle?.desktop?.bypadtechBlue}>pad</span>tech
        </p>
      </div>

      <div style={screenStyle?.desktop?.content}>
        <div>
          <div style={screenStyle?.desktop?.leftSideAboveContainer}>
            <h1 style={screenStyle?.desktop?.title}>
              Eleve sua empresa ao pr<span style={{color: '#ffbd59'}}>ó</span>ximo nível
            </h1>

            <h4 style={screenStyle?.desktop?.subtitle}>SOFTWARES. SOLUÇÕES. IDEIAS.</h4>
          </div>

          <div style={screenStyle?.desktop?.leftSideBellowContainer}>
            <div>

              <div style={screenStyle?.desktop?.consultingTextContainer}>
                <p style={screenStyle?.desktop?.consultingText}>
                  agende sua c<span style={{color: '#ffbd59'}}>o</span>nsultoria
                </p>
              </div> 

              <div style={screenStyle?.desktop?.arrowContainer}>
                <svg class="arrows">
                  <path class="a1" d="M0 0 L15 25 L30 0"></path>
                  <path class="a2" d="M0 20 L15 45 L30 20"></path>
                  <path class="a3" d="M0 40 L15 65 L30 40"></path>
                </svg>
              </div>

            </div>

            <div>
              <img src={qrcode} alt={'QR Code para email'} style={{height: '30vh'}}/>
            </div>
          </div>
        </div>

        <div style={screenStyle?.desktop?.videoContainer}>
          <ReactPlayer url={videoLevelUp} loop={true} playing controls={true} />
        </div>
      </div>
    </>
  )

  const handleTablet = () => (
    <>
      <div style={screenStyle?.tablet?.navbar}>
        <p style={screenStyle?.tablet?.bypadtech}>
          by <span style={screenStyle?.tablet?.bypadtechBlue}>pad</span>tech
        </p>
      </div>

      <div style={screenStyle?.tablet?.content}>
        <div>
          <div style={screenStyle?.tablet?.firstContainer}>
            <h1 style={screenStyle?.tablet?.title}>
              Eleve sua empresa ao pr<span style={{color: '#ffbd59'}}>ó</span>ximo nível
            </h1>

            <h4 style={screenStyle?.tablet?.subtitle}>SOFTWARES. SOLUÇÕES. IDEIAS.</h4>
          </div>

          <div style={screenStyle?.tablet?.videoContainer}>
            <ReactPlayer url={videoLevelUp} loop={true} playing controls={true} />
          </div>

          <div style={screenStyle?.tablet?.secondContainer}>
            <div>

              <div style={screenStyle?.tablet?.consultingTextContainer}>
                <p style={screenStyle?.tablet?.consultingText}>
                  agende sua c<span style={{color: '#ffbd59'}}>o</span>nsultoria
                </p>
              </div> 

              <div style={screenStyle?.tablet?.arrowContainer}>
                <svg class="arrows">
                  <path class="a1" d="M0 0 L15 25 L30 0"></path>
                  <path class="a2" d="M0 20 L15 45 L30 20"></path>
                  <path class="a3" d="M0 40 L15 65 L30 40"></path>
                </svg>
              </div>

            </div>

            <div>
              <img src={qrcode} alt={'QR Code para email'} style={{height: '30vh'}}/>
            </div>
          </div>
        </div>
      </div>
    </>
  )

  const handleMobile = () => (
    <>
      <div style={screenStyle?.mobile?.navbar}>
        <p style={screenStyle?.mobile?.bypadtech}>
          by <span style={screenStyle?.mobile?.bypadtechBlue}>pad</span>tech
        </p>
      </div>

      <div style={screenStyle?.mobile?.content}>
        <div>

          <div style={screenStyle?.mobile?.firstContainer}>
            <h1 style={screenStyle?.mobile?.title}>
              Eleve sua empresa ao pr<span style={{color: '#ffbd59'}}>ó</span>ximo nível
            </h1>

            <h4 style={screenStyle?.mobile?.subtitle}>SOFTWARES. SOLUÇÕES. IDEIAS.</h4>
          </div>

          <div style={screenStyle?.mobile?.videoContainer}>
            <ReactPlayer url={videoLevelUp} loop={true} playing controls={false} />
          </div>

          <div style={screenStyle?.mobile?.secondContainer}>
            <div>

              <div style={screenStyle?.mobile?.consultingTextContainer}>
                <p style={screenStyle?.mobile?.consultingText}>
                  agende sua c<span style={{color: '#ffbd59'}}>o</span>nsultoria
                </p>
              </div> 

              <div style={screenStyle?.mobile?.arrowContainer}>
                <svg class="arrows">
                  <path class="a1" d="M0 0 L15 25 L30 0"></path>
                  <path class="a2" d="M0 20 L15 45 L30 20"></path>
                  <path class="a3" d="M0 40 L15 65 L30 40"></path>
                </svg>
              </div>

            </div>

            <div>
              <img src={qrcode} alt={'QR Code para email'} style={{height: '30vh'}}/>
            </div>
          </div>
        </div>
      </div>
    </>
  )


  if (desktop) {
    return handleDesktop()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  } else {
    window.location.reload()
  }
}

export default Banner