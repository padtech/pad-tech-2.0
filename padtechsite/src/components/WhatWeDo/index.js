import React from 'react'
import { useStyles } from 'react-styles-hook'
import { Animated } from 'react-animated-css'

import { useResponsive } from '../../hooks/responsive'

import brain from '../../assets/images/brain.png'

import styles from './styles'
import './styles.css'

const WhatWeDo = (props) => {
  const screenStyle = useStyles(styles(props))

  const { desktop, tabletLandscape, tablet, mobile } = useResponsive()

  const handleDesktop = () => (
    <>
      <div style={screenStyle?.desktop?.titleContent}> 
        <div style={screenStyle?.desktop?.navbar}>
          <h1 style={screenStyle?.desktop?.title}><span style={{color: '#396cb2'}}>O que f</span>azemos?</h1>
          <hr style={screenStyle?.desktop?.line} />
        </div>
        
        <div style={screenStyle?.desktop?.content}>
          <div style={screenStyle?.desktop?.leftSideContent}>
            <p style={screenStyle?.desktop?.padtechText}>
              Bom, nossa missão é conseguir extrair o que estiver na sua cabeça e fabricar o software com base nisso.
            </p>

            <div style={screenStyle?.desktop?.ourfactoryScrollDownContainer}>
              <p style={screenStyle?.desktop?.ourfactoryScrollDownText}>Veja alguns exemplos do que sai da nossa fábrica</p>
              <div class="arrow">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
            
          </div>
          <div style={screenStyle?.desktop?.rightSideContent}>
            <Animated animationInDuration={2500} animationIn="slideInDown" isVisible={true}>
              <img src={brain} alt={'Cérebro'} style={{height: '80vh'}}/>
            </Animated>
          </div>
        </div>
      </div>
    </>
  )

  const handleTabletLandscape = () => (
    <>
      <div style={screenStyle?.desktop?.titleContent}> 
        <div style={screenStyle?.desktop?.navbar}>
          <h1 style={screenStyle?.desktop?.title}><span style={{color: '#396cb2'}}>O que f</span>azemos?</h1>
          <hr style={screenStyle?.desktop?.line} />
        </div>
        
        <div style={screenStyle?.desktop?.content}>
          <div style={screenStyle?.desktop?.leftSideContent}>
            <p style={screenStyle?.desktop?.padtechText}>
              Bom, nossa missão é conseguir extrair o que estiver na sua cabeça e fabricar o software com base nisso.
            </p>

            <div style={screenStyle?.desktop?.ourfactoryScrollDownContainer}>
              <p style={screenStyle?.desktop?.ourfactoryScrollDownText}>Veja alguns exemplos do que sai da nossa fábrica</p>
              <div class="arrow">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
            
          </div>
          <div style={screenStyle?.desktop?.rightSideContent}>
            <Animated animationInDuration={2500} animationIn="slideInDown" isVisible={true}>
              <img src={brain} alt={'Cérebro'} style={{height: '80vh'}}/>
            </Animated>
          </div>
        </div>
      </div>
    </>
  )

  const handleTablet = () => (
    <>
      <div style={screenStyle?.tablet?.titleContent}> 
        <div style={screenStyle?.tablet?.navbar}>
          <h1 style={screenStyle?.tablet?.title}><span style={{color: '#396cb2'}}>O que f</span>azemos?</h1>
          <hr style={screenStyle?.tablet?.line} />
        </div>
        
        <div style={screenStyle?.tablet?.content}>
          <div style={screenStyle?.tablet?.aboveContent}>
            <Animated animationInDuration={2500} animationIn="slideInDown" isVisible={true}>
              <img src={brain} alt={'Cérebro'} style={{height: '70vh'}}/>
            </Animated>
            <p style={screenStyle?.tablet?.padtechText}>
              Bom, nossa missão é conseguir extrair o que estiver na sua cabeça e fabricar o software com base nisso.
            </p>
          </div>

          <div style={screenStyle?.tablet?.belowContent}>
            <div style={screenStyle?.tablet?.ourfactoryScrollDownContainer}>
              <p style={screenStyle?.tablet?.ourfactoryScrollDownText}>Veja alguns exemplos do que sai da nossa fábrica</p>
              <div class="arrow-mobile">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )

  const handleMobile = () => (
    <>
      <div style={screenStyle?.mobile?.mainContainer}> 
        <div style={screenStyle?.mobile?.navbar}>
          <h1 style={screenStyle?.mobile?.title}><span style={{color: '#396cb2'}}>O que f</span>azemos?</h1>
          <hr style={screenStyle?.mobile?.line} />
        </div>

        <div style={screenStyle?.mobile?.aboveContent}>
            <Animated animationInDuration={2500} animationIn="slideInDown" isVisible={true}>
              <img src={brain} alt={'Cérebro'} style={{height: '50vh'}}/>
            </Animated>
          </div>
        
        <div style={screenStyle?.mobile?.content}>
          <div style={screenStyle?.mobile?.belowContent}>
            <p style={screenStyle?.mobile?.padtechText}>
              Bom, nossa missão é conseguir extrair o que estiver na sua cabeça e fabricar o software com base nisso.
            </p>

            <div style={screenStyle?.mobile?.ourfactoryScrollDownContainer}>
              <p style={screenStyle?.mobile?.ourfactoryScrollDownText}>Veja alguns exemplos do que sai da nossa fábrica</p>
              <div class="arrow-mobile">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
            
          </div>

        </div>
      </div>
    </>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  } else {
    return
  }
}

export default WhatWeDo