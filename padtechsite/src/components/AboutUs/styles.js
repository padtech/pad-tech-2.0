import { commonVariables } from '../../styles/variables'

const styles = (props) => {

  return ( 
    {
      desktop: {
        titleContent: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '100%',
          height: '10vh',
          marginTop: '30vh',
          alignItems: 'center',
          alignContent: 'flex-start',
        },

        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '48px',
          color: commonVariables?.greyPadtech,
          fontWeight: '500',
          marginTop: '0',
          marginBottom: '0',
          marginLeft: '5vw'
        },
        line: {
          width: '60%',
          size:'3',
          color: commonVariables?.greyPadtec
        },
        content: {
          display: 'flex',
          flexDirection: 'row',
          marginTop: '1vh'
        },
        leftSideContent: {
          width: '50%',
          padding: '2%'
        },
        rightSideContent: {
          width: '50%',
          padding: '2%',
          display: 'flex',
          alignItems: 'center',
          marginTop: '-10vh',
        },
        padtechText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '28px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginRight: '10vw',
        }
      },

      tablet: {
        titleContent: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '100vw',
          height: '10vh',
          marginTop: '15vh',
          alignItems: 'center',
          alignContent: 'flex-start',
        },

        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '48px',
          color: commonVariables?.greyPadtech,
          fontWeight: '800',
          textShadow: '2px 3px 5px rgba(180, 180, 180, 0.6)',
          marginTop: '0',
          marginBottom: '0',
          marginLeft: '3vw'
        },
        line: {
          width: '55%',
          size:'3',
          color: commonVariables?.greyPadtec
        },
        content: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '1vh'
        },
        aboveContent: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          width: '100vw',
          marginBottom: '10vh'
        },
        belowContent: {
          width: '100vw',
          padding: '2%',
          display: 'flex',
          alignItems: 'center',
          marginTop: '-10vh',
        },
        padtechText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '30px',
          textAlign: 'center',
          color: commonVariables?.greyPadtech,
          fontWeight: '200',
          marginRight: '5vw',
        }
      },

      mobile: {
        titleContent: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '100vw',
          height: '10vh',
          marginTop: '15vh',
          alignItems: 'center',
          alignContent: 'flex-start',
        },

        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '32px',
          color: commonVariables?.greyPadtech,
          fontWeight: '800',
          textShadow: '2px 3px 5px rgba(180, 180, 180, 0.6)',
          marginTop: '0',
          marginBottom: '0',
          marginLeft: '5vw'
        },
        line: {
          width: '45%',
          size:'3',
          color: commonVariables?.greyPadtec
        },
        content: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '1vh'
        },
        aboveContent: {
          width: '100vw',
          padding: '2%',
          marginBottom: '10vh'
        },
        belowContent: {
          width: '100vw',
          padding: '2%',
          display: 'flex',
          alignItems: 'center',
          marginTop: '-10vh',
        },
        padtechText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '26px',
          textAlign: 'center',
          color: commonVariables?.greyPadtech,
          fontWeight: '200',
          marginRight: '10vw',
        }
      },

    }
  )
}

export default styles