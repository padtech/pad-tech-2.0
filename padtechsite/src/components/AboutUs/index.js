import React from 'react'
import { useStyles } from 'react-styles-hook'

import { useResponsive } from '../../hooks/responsive'

import logo from '../../assets/images/logo.png'

import styles from './styles'

const AboutUs = (props) => {
  const screenStyle = useStyles(styles(props))
  
  const { desktop, tabletLandscape, tablet, mobile } = useResponsive()

  const handleDesktop = () => (
    <>
      <div style={screenStyle?.desktop?.titleContent}>
        <div style={screenStyle?.desktop?.navbar}>
          <h1 style={screenStyle?.desktop?.title}><span style={{color: '#ffbd59'}}>Sob</span>re nós</h1>
          <hr style={screenStyle?.desktop?.line} />
        </div>
        
        <div style={screenStyle?.desktop?.content}>
          <div style={screenStyle?.desktop?.leftSideContent}>
            <img src={logo} alt={'Logo PAD Tech'} style={{}}/>
          </div>
          <div style={screenStyle?.desktop?.rightSideContent}>
            <p style={screenStyle?.desktop?.padtechText}>
              A PAD Tech começou no momento em que percebemos a possibilidade de <span style={{color: '#ffbd59'}}>fabricar ideias</span>. Achamos incrível poder modelar a solução exatamente como está no cérebro do cliente.
            </p>
          </div>
        </div>
      </div>
    </>
  )

  const handleTabletLandscape = () => (
    <>
      <div style={screenStyle?.desktop?.titleContent}>
        <div style={screenStyle?.desktop?.navbar}>
          <h1 style={screenStyle?.desktop?.title}><span style={{color: '#ffbd59'}}>Sob</span>re nós</h1>
          <hr style={screenStyle?.desktop?.line} />
        </div>
        
        <div style={screenStyle?.desktop?.content}>
          <div style={screenStyle?.desktop?.leftSideContent}>
            <img src={logo} alt={'Logo PAD Tech'} style={{}}/>
          </div>
          <div style={screenStyle?.desktop?.rightSideContent}>
            <p style={screenStyle?.desktop?.padtechText}>
              A PAD Tech começou no momento em que percebemos a possibilidade de <span style={{color: '#ffbd59'}}>fabricar ideias</span>. Achamos incrível poder modelar a solução exatamente como está no cérebro do cliente.
            </p>
          </div>
        </div>
      </div>
    </>
  )

  const handleTablet = () => (
    <>
      <div style={screenStyle?.tablet?.titleContent}>
        <div style={screenStyle?.tablet?.navbar}>
          <h1 style={screenStyle?.tablet?.title}><span style={{color: '#ffbd59'}}>Sob</span>re nós</h1>
          <hr style={screenStyle?.tablet?.line} />
        </div>
        
        <div style={screenStyle?.tablet?.content}>
          <div style={screenStyle?.tablet?.aboveContent}>
            <img src={logo} alt={'Logo PAD Tech'} style={{height: '45vh'}}/>
          </div>
          <div style={screenStyle?.tablet?.belowContent}>
            <p style={screenStyle?.tablet?.padtechText}>
              A PAD Tech começou no momento em que percebemos a possibilidade de <span style={{color: '#ffbd59'}}>fabricar ideias</span>. Achamos incrível poder modelar a solução exatamente como está no cérebro do cliente.
            </p>
          </div>
        </div>
      </div>
    </>
  )

  const handleMobile = () => (
    <>
      <div style={screenStyle?.mobile?.titleContent}>
        <div style={screenStyle?.mobile?.navbar}>
          <h1 style={screenStyle?.mobile?.title}><span style={{color: '#ffbd59'}}>Sob</span>re nós</h1>
          <hr style={screenStyle?.mobile?.line} />
        </div>
        
        <div style={screenStyle?.mobile?.content}>
          <div style={screenStyle?.mobile?.aboveContent}>
            <img src={logo} alt={'Logo PAD Tech'} style={{height: '45vh'}}/>
          </div>
          <div style={screenStyle?.mobile?.belowContent}>
            <p style={screenStyle?.mobile?.padtechText}>
              A PAD Tech começou no momento em que percebemos a possibilidade de <span style={{color: '#ffbd59'}}>fabricar ideias</span>. Achamos incrível poder modelar a solução exatamente como está no cérebro do cliente.
            </p>
          </div>
        </div>
      </div>
    </>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  } else {
    window.location.reload()
  }
}

export default AboutUs