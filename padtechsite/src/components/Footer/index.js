import React from 'react'
import { useStyles } from 'react-styles-hook'

import footerImage from '../../assets/images/footerImage.png'
import footerImageMobile from '../../assets/images/footerImageMobile.png'

import { useResponsive } from '../../hooks/responsive'

import ContactForm from '../../components/ContactForm/index'

import styles from './styles'
import './styles.css'

const Footer = (props) => {
  const screenStyle = useStyles(styles(props))

  const { desktop, tabletLandscape, tablet, mobile } = useResponsive()

  const handleDesktop = () => (
    <div style={screenStyle?.desktop?.mainContainer}>
      <img src={footerImage} alt={'Imagem Footer'}/>
      <ContactForm />
    </div>
  )

  const handleTabletLandscape = () => (
    <div style={screenStyle?.desktop?.mainContainer}>
      <img src={footerImage} alt={'Imagem Footer'}/>
      <ContactForm />
    </div>
  )

  const handleTablet = () => (
    <div style={screenStyle?.tablet?.mainContainer}>
      <img src={footerImageMobile} alt={'Imagem Footer'}/>
      <ContactForm />
    </div>
  )

  const handleMobile = () => (
    <div style={screenStyle?.mobile?.mainContainer}>
      <img src={footerImageMobile} alt={'Imagem Footer'} style={{height: 'auto', width: 'auto'}}/>
      <ContactForm />
    </div>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  }
}

export default Footer