import { commonVariables } from '../../styles/variables'

const styles = (props) => {

  return ( 
    {
      desktop: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          marginTop: '20vh',
          overflowX: 'hidden',
          borderTopStyle: 'solid',
          borderTopWidth: '5px',
          borderTopColor: 'grey',
          boxShadow: '0px 0 60px rgba(0, 0, 0, 0.8)',
        },
        input: {
          border: 'none',
          borderBottom: '2px solid #3e70b4',
          backgroundColor: '#3e70b4',
          color: '#f5f3f4'
        }
      },

      tablet: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: '32vh',
          width: '101vw',
          maxWidth: '101vw',
          marginTop: '20vh',
          overflow: 'hidden',
          borderTopStyle: 'solid',
          borderTopWidth: '1px',
          borderTopColor: 'lightGrey',
          boxShadow: '0px 0 60px rgba(130, 130, 130, 0.8)',
        },
        input: {
          border: 'none',
          borderBottom: '2px solid #3e70b4',
          backgroundColor: '#3e70b4',
          color: '#f5f3f4'
        }
      },

      mobile: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: '40vh',
          width: '100vw',
          maxWidth: '100vw',
          marginTop: '20vh',
          overflow: 'hidden',
          borderTopStyle: 'solid',
          borderTopWidth: '1px',
          borderTopColor: 'lightGrey',
          boxShadow: '0px 0 60px rgba(130, 130, 130, 0.8)',
          marginLeft: '-2vw'
        },
        input: {
          border: 'none',
          borderBottom: '2px solid #3e70b4',
          backgroundColor: '#3e70b4',
          color: '#f5f3f4'
        }
      },


    }
  )
}

export default styles