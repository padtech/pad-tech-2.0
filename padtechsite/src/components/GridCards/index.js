import React from 'react'
import { useStyles } from 'react-styles-hook'

import Card from '../Card/index'

import { useResponsive } from '../../hooks/responsive'

import ecommerce from '../../assets/videos/ecommerce.mp4'
import design from '../../assets/videos/design.mp4'
import outsourcing from '../../assets/videos/outsourcing.mp4'
import solutions from '../../assets/videos/solutions.mp4'
import apps from '../../assets/videos/apps.mp4'
import chatbot from '../../assets/videos/chatbot.mp4'

import styles from './styles'

const GridCards = (props) => {
  const screenStyle = useStyles(styles(props))

  const { desktop, tabletLandscape, tablet, mobile } = useResponsive()

  const handleDesktop = () => (
    <div style={screenStyle?.desktop?.mainContainer}>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={ecommerce}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>E-commerce</h1>
        <div style={{width: 'auto', flexWrap: 'wrap'}}>
          <p style={screenStyle?.desktop?.cardText}>
            Você sonha em ser dono de uma loja? Aqui temos toda a estrutura para a sua posição online. Vamos fabricar?
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={design}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Design</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Conta pra gente um pouquinho da sua ideia. Vamos criar, desenvolver e entregar uma cara nova à sua empresa / marca.
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={outsourcing}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Outsourcing</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Prestamos serviços tecnológicos para qualquer empresa, ou cliente particular, no mundo. Sobretudo clientes na Europa e América.
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={solutions}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Soluções</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Sua empresa precisa de inteligência, automatização de processos ou inserção digital? Pode contar conosco!
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={apps}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Apps</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Desenvolvemos apps em iOS e Android. Em qualquer área de negócio. Está na hora de tirar aquela sua ideia do papel!
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={chatbot}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Chatbot</h1>
        <div>
          <p style={{...screenStyle?.desktop?.cardText, marginBottom: '-1vh'}}>
            Inteligência Artificial.
          </p>
          <p style={{...screenStyle?.desktop?.cardText, marginBottom: '-1vh'}}>
            Automatização.
          </p>
          <p style={screenStyle?.desktop?.cardText}>
            Imersão digital.
          </p>  
        </div>
      </div>
    </div>
  )

  const handleTabletLandscape = () => (
    <div style={screenStyle?.desktop?.mainContainer}>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={ecommerce}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>E-commerce</h1>
        <div style={{width: 'auto', flexWrap: 'wrap'}}>
          <p style={screenStyle?.desktop?.cardText}>
            Você sonha em ser dono de uma loja? Aqui temos toda a estrutura para a sua posição online. Vamos fabricar?
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={design}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Design</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Conta pra gente um pouquinho da sua ideia. Vamos criar, desenvolver e entregar uma cara nova à sua empresa / marca.
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={outsourcing}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Outsourcing</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Prestamos serviços tecnológicos para qualquer empresa, ou cliente particular, no mundo. Sobretudo clientes na Europa e América.
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={solutions}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Soluções</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Sua empresa precisa de inteligência, automatização de processos ou inserção digital? Pode contar conosco!
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={apps}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Apps</h1>
        <div>
          <p style={screenStyle?.desktop?.cardText}>
            Desenvolvemos apps em iOS e Android. Em qualquer área de negócio. Está na hora de tirar aquela sua ideia do papel!
          </p>
        </div>
      </div>
      <div style={screenStyle?.desktop?.cardContainer}>
        <Card videoUrl={chatbot}/>
        <h1 style={screenStyle?.desktop?.cardTitle}>Chatbot</h1>
        <div>
          <p style={{...screenStyle?.desktop?.cardText, marginBottom: '-1vh'}}>
            Inteligência Artificial.
          </p>
          <p style={{...screenStyle?.desktop?.cardText, marginBottom: '-1vh'}}>
            Automatização.
          </p>
          <p style={screenStyle?.desktop?.cardText}>
            Imersão digital.
          </p>  
        </div>
      </div>
    </div>
  )

  const handleTablet = () => (
    <div style={screenStyle?.tablet?.mainContainer}>
      <div style={screenStyle?.tablet?.cardContainer}>
        <Card videoUrl={ecommerce}/>
        <h1 style={screenStyle?.tablet?.cardTitle}>E-commerce</h1>
        <div style={{width: 'auto', flexWrap: 'wrap'}}>
          <p style={screenStyle?.tablet?.cardText}>
            Você sonha em ser dono de uma loja? Aqui temos toda a estrutura para a sua posição online. Vamos fabricar?
          </p>
        </div>
      </div>
      <div style={screenStyle?.tablet?.cardContainer}>
        <Card videoUrl={design}/>
        <h1 style={screenStyle?.tablet?.cardTitle}>Design</h1>
        <div>
          <p style={screenStyle?.tablet?.cardText}>
            Conta pra gente um pouquinho da sua ideia. Vamos criar, desenvolver e entregar uma cara nova à sua empresa / marca.
          </p>
        </div>
      </div>
      <div style={screenStyle?.tablet?.cardContainer}>
        <Card videoUrl={outsourcing}/>
        <h1 style={screenStyle?.tablet?.cardTitle}>Outsourcing</h1>
        <div>
          <p style={screenStyle?.tablet?.cardText}>
            Prestamos serviços tecnológicos para qualquer empresa, ou cliente particular, no mundo. Sobretudo clientes na Europa e América.
          </p>
        </div>
      </div>
      <div style={screenStyle?.tablet?.cardContainer}>
        <Card videoUrl={solutions}/>
        <h1 style={screenStyle?.tablet?.cardTitle}>Soluções</h1>
        <div>
          <p style={screenStyle?.tablet?.cardText}>
            Sua empresa precisa de inteligência, automatização de processos ou inserção digital? Pode contar conosco!
          </p>
        </div>
      </div>
      <div style={screenStyle?.tablet?.cardContainer}>
        <Card videoUrl={apps}/>
        <h1 style={screenStyle?.tablet?.cardTitle}>Apps</h1>
        <div>
          <p style={screenStyle?.tablet?.cardText}>
            Desenvolvemos apps em iOS e Android. Em qualquer área de negócio. Está na hora de tirar aquela sua ideia do papel!
          </p>
        </div>
      </div>
      <div style={screenStyle?.tablet?.cardContainer}>
        <Card videoUrl={chatbot}/>
        <h1 style={screenStyle?.tablet?.cardTitle}>Chatbot</h1>
        <div>
          <p style={{...screenStyle?.tablet?.cardText, marginBottom: '-1vh'}}>
            Inteligência Artificial.
          </p>
          <p style={{...screenStyle?.tablet?.cardText, marginBottom: '-1vh'}}>
            Automatização.
          </p>
          <p style={screenStyle?.tablet?.cardText}>
            Imersão digital.
          </p>  
        </div>
      </div>
    </div>
  )

  const handleMobile = () => (
    <div style={screenStyle?.mobile?.mainContainer}>
      <div style={screenStyle?.mobile?.cardContainer}>
        <Card videoUrl={ecommerce}/>
        <h1 style={screenStyle?.mobile?.cardTitle}>E-commerce</h1>
        <div style={{width: 'auto', flexWrap: 'wrap'}}>
          <p style={screenStyle?.mobile?.cardText}>
            Você sonha em ser dono de uma loja? Aqui temos toda a estrutura para a sua posição online. Vamos fabricar?
          </p>
        </div>
      </div>
      <div style={screenStyle?.mobile?.cardContainer}>
        <Card videoUrl={design}/>
        <h1 style={screenStyle?.mobile?.cardTitle}>Design</h1>
        <div>
          <p style={screenStyle?.mobile?.cardText}>
            Conta pra gente um pouquinho da sua ideia. Vamos criar, desenvolver e entregar uma cara nova à sua empresa / marca.
          </p>
        </div>
      </div>
      <div style={screenStyle?.mobile?.cardContainer}>
        <Card videoUrl={outsourcing}/>
        <h1 style={screenStyle?.mobile?.cardTitle}>Outsourcing</h1>
        <div>
          <p style={screenStyle?.mobile?.cardText}>
            Prestamos serviços tecnológicos para qualquer empresa, ou cliente particular, no mundo. Sobretudo clientes na Europa e América.
          </p>
        </div>
      </div>
      <div style={screenStyle?.mobile?.cardContainer}>
        <Card videoUrl={solutions}/>
        <h1 style={screenStyle?.mobile?.cardTitle}>Soluções</h1>
        <div>
          <p style={screenStyle?.mobile?.cardText}>
            Sua empresa precisa de inteligência, automatização de processos ou inserção digital? Pode contar conosco!
          </p>
        </div>
      </div>
      <div style={screenStyle?.mobile?.cardContainer}>
        <Card videoUrl={apps}/>
        <h1 style={screenStyle?.mobile?.cardTitle}>Apps</h1>
        <div>
          <p style={screenStyle?.mobile?.cardText}>
            Desenvolvemos apps em iOS e Android. Em qualquer área de negócio. Está na hora de tirar aquela sua ideia do papel!
          </p>
        </div>
      </div>
      <div style={screenStyle?.mobile?.cardContainer}>
        <Card videoUrl={chatbot}/>
        <h1 style={screenStyle?.mobile?.cardTitle}>Chatbot</h1>
        <div>
          <p style={{...screenStyle?.mobile?.cardText, marginBottom: '-1vh'}}>
            Inteligência Artificial.
          </p>
          <p style={{...screenStyle?.mobile?.cardText, marginBottom: '-1vh'}}>
            Automatização.
          </p>
          <p style={screenStyle?.mobile?.cardText}>
            Imersão digital.
          </p>  
        </div>
      </div>
    </div>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  } else {
    return
  }
}

export default GridCards