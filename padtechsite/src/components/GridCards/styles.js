import { commonVariables } from '../../styles/variables'

const styles = (props) => {

  return ( 
    {
      desktop: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden',
          justifyContent: 'center',
          marginTop: '5vh'
        },

        cardContainer: {
          display: 'flex',
          flexDirection: 'column',
          marginLeft: '5%',
          marginRight: '5%',
          height: '80vh',
          width: '20vw',
          justifyContent: 'center',
          alignItems: 'center',
        },
        cardTitle: {
          fontFamily: commonVariables?.padtech,
          color: commonVariables?.greyPadtech,
          marginBottom: '1vh',
          fontWeight: '200',
          zIndex: 10
        },
        cardText: {
          fontFamily: commonVariables?.padtech,
          color: commonVariables?.greyPadtech,
          fontWeight: '200',
          zIndex: 10,
          textAlign: 'center'
        }
      },

      tablet: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden',
          justifyContent: 'center',
          marginTop: '5vh',
        },

        cardContainer: {
          display: 'flex',
          flexDirection: 'column',
          marginLeft: '5%',
          marginRight: '5%',
          height: '80vh',
          width: '100vw',
          justifyContent: 'center',
          alignItems: 'center',
        },
        cardTitle: {
          fontFamily: commonVariables?.padtech,
          color: commonVariables?.greyPadtech,
          marginBottom: '1vh',
          fontSize: '62px',
          fontWeight: '300',
          zIndex: 10
        },
        cardText: {
          fontFamily: commonVariables?.padtech,
          color: commonVariables?.greyPadtech,
          fontSize: '32px',
          fontWeight: '200',
          zIndex: 10,
          textAlign: 'center',
          marginBottom: '7vh'
        }
      },

      mobile: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'wrap',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden',
          justifyContent: 'center',
          marginTop: '5vh',
        },

        cardContainer: {
          display: 'flex',
          flexDirection: 'column',
          marginLeft: '5%',
          marginRight: '5%',
          height: '80vh',
          width: '100vw',
          justifyContent: 'center',
          alignItems: 'center',
        },
        cardTitle: {
          fontFamily: commonVariables?.padtech,
          color: commonVariables?.greyPadtech,
          marginBottom: '1vh',
          fontSize: '48px',
          fontWeight: '300',
          zIndex: 10
        },
        cardText: {
          fontFamily: commonVariables?.padtech,
          color: commonVariables?.greyPadtech,
          fontSize: '22px',
          fontWeight: '200',
          zIndex: 10,
          textAlign: 'center',
          marginBottom: '10vh'
        }
      },


    }
  )
}

export default styles