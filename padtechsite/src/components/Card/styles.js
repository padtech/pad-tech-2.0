import { commonVariables } from '../../styles/variables'

const styles = (props) => {

  return ( 
    {
      desktop: {
        cardContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: '40vh',
          margin: '5%',
          width: '20vw',
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          boxShadow: '1px 1px 7px rgba(0, 0, 0, 0.6)',
          borderRadius: '15px'
        },
      },

      tablet: {
        cardContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: '40vh',
          margin: '5%',
          width: '60vw',
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          boxShadow: '1px 6px 8px rgba(180, 180, 180, 0.8)',
          borderRadius: '15px'
        },
      },

      mobile: {
        cardContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: '34vh',
          margin: '5%',
          width: '75vw',
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          boxShadow: '1px 6px 8px rgba(180, 180, 180, 0.8)',
        },
      },
    }
  )
}

export default styles