import React from 'react'
import ReactPlayer from 'react-player'

import { useStyles } from 'react-styles-hook'

import { useResponsive } from '../../hooks/responsive'

import styles from './styles'

const Card = (props) => {
  const cardStyle = useStyles(styles(props))

  const { desktop, tabletLandscape, tablet, mobile} = useResponsive()

  const { videoUrl } = props

  const handleDesktop = () => (
    <div style={cardStyle?.desktop?.cardContainer}>
      <ReactPlayer url={videoUrl} loop playing controls={true} />
    </div>
  )

  const handleTabletLandscape = () => (
    <div style={cardStyle?.desktop?.cardContainer}>
      <ReactPlayer url={videoUrl} loop playing controls={true} />
    </div>
  )

  const handleTablet = () => (
    <div style={cardStyle?.tablet?.cardContainer}>
      <ReactPlayer url={videoUrl} loop playing controls={true} height={'460px'} />
    </div>
  )

  const handleMobile = () => (
    <div style={cardStyle?.mobile?.cardContainer}>
      <ReactPlayer url={videoUrl} loop playing controls={true}/>
    </div>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tablet) {
    return handleTablet()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (mobile) {
    return handleMobile()
  }
}

export default Card