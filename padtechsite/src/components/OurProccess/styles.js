import { commonVariables } from '../../styles/variables'

const styles = (props) => {

  return ( 
    {
      desktop: {
        titleContent: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '100%',
          height: '10vh',
          marginTop: '15vh',
          alignItems: 'center',
          alignContent: 'flex-start',
        },

        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '48px',
          color: commonVariables?.greyPadtech,
          fontWeight: '500',
          marginTop: '0',
          marginBottom: '0',
          marginLeft: '5vw'
        },
        line: {
          width: '55%',
          size:'3',
          color: commonVariables?.greyPadtec
        },
        content: {
          display: 'flex',
          flexDirection: 'row',
          marginTop: '1vh'
        },
        rightSideContent: {
          width: '50%',
          padding: '2%',
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
        },
        leftSideContent: {
          width: '50%',
          padding: '2%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '10vh',
        },
        padtechText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '34px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginLeft: '5vw',
        },
        fullScreenText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '14px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginLeft: '18vw',
        },
        ourfactoryScrollDownContainer: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '5vh'
        },
        ourfactoryScrollDownText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '22px',
          color: commonVariables?.greyPadtech,
          fontWeight: '600',
          marginLeft: '23vw',
          textAlign: 'center'
        }
      },

      tablet: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden',
          marginLeft: '-1vw'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '100vw',
          height: '10vh',
          marginTop: '5vh',
          alignItems: 'center',
          alignContent: 'flex-start',
        },

        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '48px',
          color: commonVariables?.greyPadtech,
          fontWeight: '800',
          textShadow: '2px 3px 5px rgba(180, 180, 180, 0.6)',
          marginTop: '0',
          marginBottom: '0',
          marginLeft: '3vw'
        },
        line: {
          width: '40%',
          size:'3',
          color: commonVariables?.greyPadtec
        },
        content: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '1vh'
        },
        aboveContent: {
          width: '100vw',
          padding: '2%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '2vh',
          marginBottom: '5vh'
        },
        belowContent: {
          width: '100vw',
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
        },
        padtechText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '42px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginRight: '2.5vw',
          textAlign: 'center'
        },
        fullScreenText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '18px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginLeft: '20vw',
        },
        ourfactoryScrollDownContainer: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '5vh'
        },
        ourfactoryScrollDownText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '22px',
          color: commonVariables?.greyPadtech,
          fontWeight: '600',
          marginLeft: '23vw',
          textAlign: 'center'
        }
      },

      mobile: {
        mainContainer: {
          display: 'flex',
          flexDirection: 'column',
          height: 'auto',
          width: '100vw',
          maxWidth: '100vw',
          overflowX: 'hidden',
          marginLeft: '-1vw'
        },
        navbar: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: '100vw',
          height: '10vh',
          marginTop: '5vh',
          alignItems: 'center',
          alignContent: 'flex-start',
        },

        title: {
          fontFamily: commonVariables?.padtech,
          fontSize: '32px',
          color: commonVariables?.greyPadtech,
          fontWeight: '800',
          textShadow: '2px 3px 5px rgba(180, 180, 180, 0.6)',
          marginTop: '0',
          marginBottom: '0',
          marginLeft: '5vw'
        },
        line: {
          width: '25%',
          size:'3',
          color: commonVariables?.greyPadtec
        },
        content: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '1vh'
        },
        belowContent: {
          width: '100vw',
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
        },
        aboveContent: {
          width: '100vw',
          padding: '2%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '2vh',
        },
        padtechText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '28px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginRight: '2.5vw',
          textAlign: 'center'
        },
        fullScreenText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '14px',
          color: commonVariables?.greyPadtech,
          fontWeight: '300',
          marginLeft: '18vw',
        },
        ourfactoryScrollDownContainer: {
          display: 'flex',
          flexDirection: 'column',
          marginTop: '5vh'
        },
        ourfactoryScrollDownText: {
          fontFamily: commonVariables?.padtech,
          fontSize: '22px',
          color: commonVariables?.greyPadtech,
          fontWeight: '600',
          marginLeft: '23vw',
          textAlign: 'center'
        }
      },


    }
  )
}

export default styles