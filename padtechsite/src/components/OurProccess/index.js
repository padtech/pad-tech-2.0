import React from 'react'
import ReactPlayer from 'react-player'
import { useStyles } from 'react-styles-hook'

import { useResponsive } from '../../hooks/responsive'

import comercial from '../../assets/videos/comercial.mp4'

import styles from './styles'

const OurProccess = (props) => {
  const screenStyle = useStyles(styles(props))

  const { desktop, tabletLandscape, tablet, mobile} = useResponsive()
  
  const handleDesktop = () => (
    <>
      <div style={screenStyle?.desktop?.titleContent}> 
        <div style={screenStyle?.desktop?.navbar}>
          <h1 style={screenStyle?.desktop?.title}><span style={{color: '#396cb2'}}>Nosso p</span>rocesso</h1>
          <hr style={screenStyle?.desktop?.line} />
        </div>
        
        <div style={screenStyle?.desktop?.content}>
          <div style={screenStyle?.desktop?.leftSideContent}>
            <p style={screenStyle?.desktop?.padtechText}>
              Criamos um vídeo super explicativo para que não fique nenhuma dúvida, <span style={{color: '#ffbd59'}}>assista</span>!
            </p>
            
          </div>
          <div style={screenStyle?.desktop?.rightSideContent}>
            <ReactPlayer url={comercial} loop={false} playing={false} controls={true} height={480} />
            <p style={screenStyle?.desktop?.fullScreenText}>Você pode ver em tela cheia também ☝🏻</p>
          </div>
        </div>
      </div>
    </>
  )

  const handleTabletLandscape = () => (
    <>
      <div style={screenStyle?.desktop?.titleContent}> 
        <div style={screenStyle?.desktop?.navbar}>
          <h1 style={screenStyle?.desktop?.title}><span style={{color: '#396cb2'}}>Nosso p</span>rocesso</h1>
          <hr style={screenStyle?.desktop?.line} />
        </div>
        
        <div style={screenStyle?.desktop?.content}>
          <div style={screenStyle?.desktop?.leftSideContent}>
            <p style={screenStyle?.desktop?.padtechText}>
              Criamos um vídeo super explicativo para que não fique nenhuma dúvida, <span style={{color: '#ffbd59'}}>assista</span>!
            </p>
            
          </div>
          <div style={screenStyle?.desktop?.rightSideContent}>
            <ReactPlayer url={comercial} loop={false} playing={false} controls={true} height={480} />
            <p style={screenStyle?.desktop?.fullScreenText}>Você pode ver em tela cheia também ☝🏻</p>
          </div>
        </div>
      </div>
    </>
  )

  const handleTablet = () => (
    <>
      <div style={screenStyle?.tablet?.titleContent}> 
        <div style={screenStyle?.tablet?.navbar}>
          <h1 style={screenStyle?.tablet?.title}><span style={{color: '#396cb2'}}>Nosso p</span>rocesso</h1>
          <hr style={screenStyle?.tablet?.line} />
        </div>
        
        <div style={screenStyle?.tablet?.content}>
          <div style={screenStyle?.tablet?.aboveContent}>
            <p style={screenStyle?.tablet?.padtechText}>
              Criamos um vídeo super explicativo para que não fique nenhuma dúvida, <span style={{color: '#ffbd59'}}>assista</span>!
            </p>
            
          </div>
          <div style={screenStyle?.tablet?.belowContent}>
            <ReactPlayer url={comercial} loop={false} playing={false} controls={true} height={480} />
            <p style={screenStyle?.tablet?.fullScreenText}>Você pode ver em tela cheia também ☝🏻</p>
          </div>
        </div>
      </div>
    </>
  )

  const handleMobile = () => (
    <>
      <div style={screenStyle?.mobile?.mainContainer}> 
        <div style={screenStyle?.mobile?.navbar}>
          <h1 style={screenStyle?.mobile?.title}><span style={{color: '#396cb2'}}>Nosso p</span>rocesso</h1>
          <hr style={screenStyle?.mobile?.line} />
        </div>
        
        <div style={screenStyle?.mobile?.content}>
          <div style={screenStyle?.mobile?.aboveContent}>
            <p style={screenStyle?.mobile?.padtechText}>
              Criamos um vídeo super explicativo para que não fique nenhuma dúvida, <span style={{color: '#ffbd59'}}>assista</span>!
            </p>
            
          </div>
          <div style={screenStyle?.mobile?.belowContent}>
            <ReactPlayer url={comercial} loop={false} playing={false} controls={false} height={360} />
          </div>
        </div>
      </div>
    </>
  )

  if (desktop) {
    return handleDesktop()
  } else if (tabletLandscape) {
    return handleTabletLandscape()
  } else if (tablet) {
    return handleTablet()
  } else if (mobile) {
    return handleMobile()
  } else {
    return
  }
}

export default OurProccess