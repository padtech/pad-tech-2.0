import { useMediaQuery } from 'react-responsive'
import { useMemo } from 'react'

export const useResponsive = () => {


  const desktop = useMediaQuery({ minWidth: 1220 })
  const tabletLandscape = useMediaQuery({ minWidth: 992, maxWidth: 1220 })
  const tablet = useMediaQuery({ minWidth: 668, maxWidth: 991 })
  const mobile = useMediaQuery({ minWidth: 275, maxWidth: 667 })


  return { desktop, tabletLandscape, tablet, mobile}
}